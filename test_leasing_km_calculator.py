from app.leasing_km_calculator import date_diff

datetimeFormat = '%d.%m.%Y'

def test_calc():
    """
    Test if the calculation is correct"
    """
    assert date_diff('05.09.2017','06.09.2017',datetimeFormat) == 1